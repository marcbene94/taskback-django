from django.db import connection
from django.http import JsonResponse
from django.http.response import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core.files.storage import FileSystemStorage
import os

@csrf_exempt
def getAllData(request):
    cursor_lists = connection.cursor()

    cursor_lists.execute("SELECT id,title FROM task.lista where active=1 order by id asc")

    result_lists = cursor_lists.fetchall()

    cursor_cards = connection.cursor()

    cursor_cards.execute("SELECT id,title,text,list FROM task.tarjeta where active=1 order by id asc")

    result_cards = cursor_cards.fetchall()

    response = {
        'data_lists': result_lists,
        'data_cards': result_cards
    }

    return JsonResponse(response)

@csrf_exempt
def uploadImageCard(request, idcard):
    if request.method == 'POST':
        # input_image = 'input-upload-file-{}'.format(idcard)
        image = request.POST.get('data')
        os.path.join('E:\django\\task\static\img\card', image)  
        # cursor = connection.cursor()
        # cursor.execute("update task.tarjeta set image=%s where id=%s", [file_url, idcard])

        return HttpResponse('Todo bien')
    return HttpResponse('todo bien2')

def getAllDeleted(request):
    cursor_lists = connection.cursor()

    cursor_lists.execute("SELECT id,title FROM task.lista where active=0 order by id asc")

    result_lists = cursor_lists.fetchall()

    cursor_cards = connection.cursor()

    cursor_cards.execute("SELECT id,title,text,list FROM task.tarjeta where active=0 order by id asc")

    result_cards = cursor_cards.fetchall()

    response = {
        'data_lists_deleted': result_lists,
        'data_cards_deleted': result_cards
    }

    return JsonResponse(response)

@csrf_exempt
def getLastListId(request):
    cursor = connection.cursor()

    cursor.execute("SELECT id FROM task.lista order by id desc limit 1")

    result = cursor.fetchone()

    response = {
        'data': result
    }

    return JsonResponse(response)

def getLastCardId(request):
    cursor = connection.cursor()

    cursor.execute("SELECT id FROM task.tarjeta order by id desc limit 1")

    result = cursor.fetchone()

    response = {
        'data': result
    }

    return JsonResponse(response)

@csrf_exempt
def saveDataNewList(request):
    data_to_save_in_database = []

    if request.method == 'POST':
        data_to_save = request.POST.get('data')
        data_to_save_in_database.append(data_to_save)

        cursor = connection.cursor()
        cursor.execute("INSERT INTO task.lista(id,title, active) VALUES( null , %s, 1 )", [data_to_save])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)

@csrf_exempt
def saveDataNewCard(request, idlist):
    data_to_save_in_database = []

    if request.method == 'POST':
        data_to_save = request.POST.get('data')
        data_to_save_in_database.append(data_to_save)

        cursor = connection.cursor()
        cursor.execute("INSERT INTO task.tarjeta(id,title,text,list,active) VALUES( null , %s,'',%s,1)", 
            [data_to_save,idlist])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)

@csrf_exempt
def saveDataTextarea(request, idcard):
    data_to_save_in_database = []

    if request.method == 'POST':
        data_to_save = request.POST.get('data')
        # int_idcard = int.from_bytes(idcard)
        data_to_save_in_database.append(data_to_save)

        cursor = connection.cursor()
        cursor.execute("update task.tarjeta set text=%s where id=%s", [data_to_save, idcard])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)

@csrf_exempt
def desactiveCard(request):
    data_to_save_in_database = []

    if request.method == 'POST':
        data_to_save = request.POST.get('data')
        # int_idcard = int.from_bytes(idcard)
        data_to_save_in_database.append(data_to_save)

        cursor = connection.cursor()
        cursor.execute("update task.tarjeta set active=0 where id=%s", [data_to_save])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)

@csrf_exempt
def desactiveList(request):
    data_to_save_in_database = []

    if request.method == 'POST':
        data_to_save = request.POST.get('data')
        # int_idcard = int.from_bytes(idcard)
        data_to_save_in_database.append(data_to_save)

        cursor = connection.cursor()
        cursor.execute("update task.lista set active=0 where id=%s", [data_to_save])
        cursor.execute("update task.tarjeta set active=0 where list=%s", [data_to_save])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)

@csrf_exempt
def updateCardAndListToReplaceCard(request, idlist, idcard):
    data_to_save_in_database = []

    if request.method == 'POST':
        id_card = request.POST.get('data')
        # int_idcard = int.from_bytes(idcard)
        data_to_save_in_database.append('SI')

        cursor = connection.cursor()
        cursor.execute("update task.tarjeta set list=%s where id=%s", [idlist, idcard])
    else:
        data_to_save_in_database.append('NO')

    return HttpResponse(data_to_save_in_database)