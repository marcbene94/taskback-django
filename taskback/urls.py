"""taskback URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls.static import static
from django.conf import settings
from taskback.views import getLastListId,saveDataNewList, getLastCardId, saveDataNewCard
from taskback.views import saveDataTextarea,desactiveCard, getAllData, desactiveList, updateCardAndListToReplaceCard, getAllDeleted
from taskback.views import uploadImageCard

urlpatterns = [
    path('admin/', admin.site.urls),
    path('getLastListId/', getLastListId),
    path('saveDataNewList/', saveDataNewList),
    path('getLastCardId/', getLastCardId),
    path('saveDataNewCard/<int:idlist>', saveDataNewCard),
    path('saveDataTextarea/<int:idcard>', saveDataTextarea),
    path('desactiveCard/', desactiveCard),
    path('getAllData/', getAllData),
    path('getAllDeleted/', getAllDeleted),
    path('desactiveList/', desactiveList),
    path('updateCardAndListToReplaceCard/<int:idlist>/<int:idcard>', updateCardAndListToReplaceCard),
    path('uploadImageCard/<int:idcard>', uploadImageCard),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)